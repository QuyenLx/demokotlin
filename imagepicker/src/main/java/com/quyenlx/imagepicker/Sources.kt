package com.quyenlx.imagepicker

/**
 * Created by QuyenLx on 6/15/2017.
 */
enum class Sources {
    CAMERA, GALLERY
}