package com.quyenlx.imagepicker

import android.content.Context
import io.reactivex.subjects.PublishSubject

/**
 * Created by QuyenLx on 6/29/2017.
 */
class RxPermissionCompat private constructor(val context: Context) {
    companion object {
        private var instance: RxPermissionCompat? = null
        fun with(context: Context): RxPermissionCompat {
            if (instance == null) {
                instance = RxPermissionCompat(context)
            }
            return instance as RxPermissionCompat
        }
    }

    private var publishSubject: PublishSubject<Boolean>? = null
    private var mPermissions: Array<String>? = null
    lateinit private var callback: () -> Unit

    private fun request(): RxPermissionCompat {
        publishSubject = PublishSubject.create()
        if (mPermissions == null || mPermissions?.size == 0) {
            val text = if (mPermissions == null) "mPermissions is null"
            else "mPermissions is empty"
            throw NullPointerException(text)
        }
        HiddenPermissionActivity.start(context, mPermissions!!)
        publishSubject?.subscribe({ value -> if (value) callback() })
        return this
    }

    fun responseSuccess(value: Boolean) {
        publishSubject?.onNext(value)
        publishSubject?.onComplete()
    }


    class Builder(context: Context) {
        private var rxPermission: RxPermissionCompat? = null

        init {
            rxPermission = with(context)
        }

        fun addPermissions(permission: Array<String>): Builder {
            rxPermission?.mPermissions = permission
            return this
        }

        fun addRequestPermissionCallback(callback: () -> Unit): Builder {
            rxPermission?.callback = callback
            return this
        }

        fun build(): RxPermissionCompat? {
            return rxPermission?.request()
        }

    }


}