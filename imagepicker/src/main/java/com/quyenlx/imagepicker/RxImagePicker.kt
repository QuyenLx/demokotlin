package com.quyenlx.imagepicker

import android.annotation.TargetApi
import android.content.Context
import android.net.Uri
import android.os.Build
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

/**
 * Created by QuyenLx on 6/15/2017.
 */
class RxImagePicker private constructor(context: Context) {
    var mPubSub: PublishSubject<Uri>? = null
    var mPubSubMultipleImages: PublishSubject<List<Uri>>? = null
    var mContext: Context? = null

    companion object {
        private var instance: RxImagePicker? = null
        fun with(context: Context): RxImagePicker {
            if (instance == null)
                instance = RxImagePicker(context.applicationContext)
            return instance as RxImagePicker
        }
    }

    init {
        mContext = context
    }

    fun requestImage(imageSource: Sources): Observable<Uri>? {
        mPubSub = PublishSubject.create<Uri>()
        HiddenActivity.start(mContext!!, imageSource.ordinal, false)
        return mPubSub
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    fun requestMultipleImages(): Observable<List<Uri>>? {
        mPubSubMultipleImages = PublishSubject.create<List<Uri>>()
        HiddenActivity.start(mContext!!, Sources.GALLERY.ordinal, true)
        return mPubSubMultipleImages
    }

    fun onImagePicked(uri: Uri?) {
        if (mPubSub != null) {
            mPubSub?.onNext(uri)
            mPubSub?.onComplete()
        }
    }

    fun onImagesPicked(uris: List<Uri>) {
        if (mPubSubMultipleImages != null) {
            mPubSubMultipleImages?.onNext(uris)
            mPubSubMultipleImages?.onComplete()
        }
    }
}