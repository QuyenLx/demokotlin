package com.quyenlx.imagepicker

import android.Manifest
import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.support.annotation.RequiresApi
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.SparseArray
import android.view.View
import java.text.SimpleDateFormat
import java.util.*

class HiddenActivity : AppCompatActivity() {

    private val KEY_CAMERA_PICTURE_URL = "camera_picture_url"
    private val SELECT_PHOTO = 100
    private val TAKE_PHOTO = 101
    private val REQUEST_PERMISSION_CAMERA = 102
    private val REQUEST_PERMISSION_GALLERY = 103

    private var cameraPictureUrl: Uri? = null
    private var mErrorString: SparseArray<Int>? = null

    companion object {
        val IMAGE_SOURCE = "image_source"
        val ALLOW_MULTIPLE_IMAGES = "allow_multiple_images"
        fun start(context: Context, imageSource: Int, allowMultipleImages: Boolean) {
            val intent = Intent(context, HiddenActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra(ALLOW_MULTIPLE_IMAGES, allowMultipleImages)
            intent.putExtra(IMAGE_SOURCE, imageSource)
            context.startActivity(intent)
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            handleIntent(intent)
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.putParcelable(KEY_CAMERA_PICTURE_URL, cameraPictureUrl)
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        cameraPictureUrl = savedInstanceState?.getParcelable(KEY_CAMERA_PICTURE_URL)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        handleIntent(intent)
    }

    fun handleIntent(intent: Intent?) {
        val sourceType = Sources.values()[intent?.getIntExtra(IMAGE_SOURCE, 0) as Int]
        when (sourceType) {
            Sources.CAMERA -> requestAppPermissions(arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE),
                    R.string.request_message_permission,
                    REQUEST_PERMISSION_CAMERA)
            Sources.GALLERY -> requestAppPermissions(
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    R.string.request_message_permission,
                    REQUEST_PERMISSION_GALLERY
            )
        }
    }


    fun requestAppPermissions(requestedPermission: Array<String>, stringId: Int, requestCode: Int) {
        mErrorString?.put(requestCode, stringId)
        var permissionCheck = PackageManager.PERMISSION_GRANTED
        var showRequestPermissions = false
        for (permission in requestedPermission) {
            permissionCheck += ContextCompat.checkSelfPermission(this, permission)
            showRequestPermissions = showRequestPermissions || ActivityCompat.shouldShowRequestPermissionRationale(this, permission)
        }
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            if (showRequestPermissions) {
                val snack = Snackbar.make(this.findViewById(android.R.id.content), mErrorString?.get(requestCode) as Int, Snackbar.LENGTH_SHORT)
                snack.setAction("GRANT", { ActivityCompat.requestPermissions(this, requestedPermission, requestCode) })
            } else {
                ActivityCompat.requestPermissions(this, requestedPermission, requestCode)
            }
        } else {
            onPermissionGranted(requestCode)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        val permissionCheck = PackageManager.PERMISSION_GRANTED + grantResults.sum()

        if (grantResults.size > 0 && PackageManager.PERMISSION_GRANTED == permissionCheck) {
            onPermissionGranted(requestCode)
        } else {
            Snackbar.make(findViewById(android.R.id.content), mErrorString?.get(requestCode) as Int, Snackbar.LENGTH_LONG)
                    .setAction("ENABLE") {
                        val i = Intent()
                        i.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                        i.data = Uri.parse("package:" + packageName)
                        i.addCategory(Intent.CATEGORY_DEFAULT)
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
                        startActivity(i)
                    }.show()
        }
    }

    fun onPermissionGranted(requestCode: Int) {
        var pictureChooseIntent: Intent? = null
        var chooseCode = when (requestCode) {
            REQUEST_PERMISSION_CAMERA -> {
                cameraPictureUrl = createImageUri()
                pictureChooseIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                pictureChooseIntent.putExtra(MediaStore.EXTRA_OUTPUT, cameraPictureUrl)
                TAKE_PHOTO
            }
            REQUEST_PERMISSION_GALLERY -> {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    pictureChooseIntent = Intent(Intent.ACTION_OPEN_DOCUMENT)
                    pictureChooseIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, intent?.getBooleanExtra(ALLOW_MULTIPLE_IMAGES, false))
                    pictureChooseIntent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION)
                } else {
                    pictureChooseIntent = Intent(Intent.ACTION_GET_CONTENT)
                }
                pictureChooseIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true)
                pictureChooseIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                pictureChooseIntent.type = "image/*"
                SELECT_PHOTO
            }

            else -> -1
        }
        startActivityForResult(pictureChooseIntent, chooseCode)
    }

    private fun createImageUri(): Uri {
        val contentResolver = contentResolver
        val contentValues = ContentValues()
        val timeStamp = SimpleDateFormat("yyyMMdd_HHmmss", Locale.getDefault()).format(Date())
        contentValues.put(MediaStore.Images.Media.TITLE, timeStamp)
        return contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues)
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                SELECT_PHOTO -> handleGalleryResult(data)
                TAKE_PHOTO -> cameraPictureUrl?.let { RxImagePicker.with(this).onImagePicked(it) }
            }
        }
        finish()
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    private fun handleGalleryResult(data: Intent?) {
        if (intent.getBooleanExtra(ALLOW_MULTIPLE_IMAGES, false)) {
            val imageUris = ArrayList<Uri>()
            val clipData = data?.clipData
            if (clipData != null) {
                for (i in 0..clipData.itemCount - 1) {
                    imageUris.add(clipData.getItemAt(i).uri)
                }
            } else {
                imageUris.add(data?.data as Uri)
                RxImagePicker.with(this).onImagesPicked(imageUris)
            }
        } else {
            RxImagePicker.with(this).onImagePicked(data?.data as Uri)
        }
    }
}
