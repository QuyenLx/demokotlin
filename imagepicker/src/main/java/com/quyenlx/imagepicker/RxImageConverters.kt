package com.quyenlx.imagepicker

import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.provider.MediaStore
import io.reactivex.Observable
import io.reactivex.ObservableOnSubscribe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream

/**
 * Created by QuyenLx on 6/15/2017.
 */
class RxImageConverters {
    fun uri2File(context: Context, uri: Uri, file: File): Observable<File> {
        return Observable
                .create(ObservableOnSubscribe<File> { emitter ->
                    try {
                        val inputStream = context.contentResolver.openInputStream(uri)
                        copyInputStreamToFile(inputStream, file)
                        emitter.onNext(file)
                        emitter.onComplete()
                    } catch (e: Exception) {
                        emitter.onError(e)
                    }
                })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun uri2Bitmap(context: Context, uri: Uri): Observable<Bitmap> {
        return Observable
                .create(ObservableOnSubscribe<Bitmap> { emitter ->
                    try {
                        val bitmap = MediaStore.Images.Media.getBitmap(context.contentResolver, uri)
                        emitter.onNext(bitmap)
                        emitter.onComplete()
                    } catch (e: IOException) {
                        emitter.onError(e)
                    }
                })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
    }


    @Throws(IOException::class)
    private fun copyInputStreamToFile(input: InputStream, file: File) {
        val out = FileOutputStream(file)
        val buf = ByteArray(10 * 1024)
        var len: Int = input.read(buf)
        while (len > 0) {
            out.write(buf, 0, len)
            len = input.read(buf)
        }
        out.close()
        input.close()
    }
}