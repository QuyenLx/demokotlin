package com.quyenlx.imagepicker

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.util.SparseArray

/**
 * Created by QuyenLx on 6/29/2017.
 */
class HiddenPermissionActivity : AppCompatActivity() {


    private val REQUEST_CODE = 1000

    private val KEY_RESPONSE_INTENT = "responseIntent"

    private var mErrorString: SparseArray<String>? = null
    private var mPermissions: Array<String>? = null
    private var mExplain: String = "Please enable all permission"

    private var responseIntent: Intent? = null

    companion object {
        val PERMISSIONS = "permissions"
        fun start(mContext: Context, mPermissions: Array<String>) {
            val intent = Intent(mContext, HiddenPermissionActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            val bundle = Bundle()
            bundle.putStringArray(PERMISSIONS, mPermissions)
            intent.putExtras(bundle)
            mContext.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            handleIntent(intent)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putParcelable(KEY_RESPONSE_INTENT, responseIntent)
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        responseIntent = savedInstanceState.getParcelable<Intent>(KEY_RESPONSE_INTENT)
    }

    override fun onNewIntent(intent: Intent) {
        handleIntent(getIntent())
    }

    private fun handleIntent(intent: Intent) {
        val extras = intent.extras
        mPermissions = extras.getStringArray(PERMISSIONS)
        requestAppPermissions(mPermissions!!, mExplain, REQUEST_CODE)
    }

    //region Handle Permission
    private fun requestAppPermissions(requestedPermission: Array<String>, explain: String?, requestCode: Int) {
        mErrorString?.put(requestCode, explain)
        var permissionCheck = PackageManager.PERMISSION_GRANTED
        var showRequestPermissions = false
        for (permission in requestedPermission) {
            permissionCheck += ContextCompat.checkSelfPermission(this, permission)
            showRequestPermissions = showRequestPermissions || ActivityCompat.shouldShowRequestPermissionRationale(this, permission)
        }
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, requestedPermission, requestCode)
        } else {
            onPermissionGranted(requestCode)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        val permissionCheck = PackageManager.PERMISSION_GRANTED + grantResults.sum()

        if (grantResults.size > 0 && PackageManager.PERMISSION_GRANTED == permissionCheck) {
            onPermissionGranted(requestCode)
        } else {
            finish()
        }
    }

    private fun onPermissionGranted(requestCode: Int) {
        if (requestCode == REQUEST_CODE) {
            RxPermissionCompat.with(this).responseSuccess(true)
        }
        finish()
    }
    //endregion

}