package com.quyenlx.demokolin.base.presenter

import com.quyenlx.demokolin.base.BaseView

/**
 * Created by QuyenLx on 6/13/2017.
 */
abstract class BasePresenterImp<V : BaseView> : BasePresenter<V> {
    protected var view: V? = null
    override fun onViewAttached(view: V) {
        this.view = view
    }

    override fun onViewDetached() {
        this.view = null
    }
}