package com.quyenlx.demokolin.base.fragment

import android.os.Bundle
import android.support.v4.app.LoaderManager
import android.support.v4.content.Loader
import com.quyenlx.demokolin.base.BaseView
import com.quyenlx.demokolin.base.activities.BaseActivity
import com.quyenlx.demokolin.base.loader.PresenterFactory
import com.quyenlx.demokolin.base.loader.PresenterLoader
import com.quyenlx.demokolin.base.presenter.BasePresenter
import java.util.concurrent.atomic.AtomicBoolean

/**
 * Created by QuyenLx on 6/13/2017.
 */
abstract class BaseFragmentInject<P : BasePresenter<V>, in V> : BaseFragment(), BaseView, LoaderManager.LoaderCallbacks<P> {
    protected var presenter: P? = null
    private var loaderId = 0
    private var firstStart = true
    private var needToCallStart = AtomicBoolean(false)


    abstract fun injectDependencies()
    abstract fun presenterFactory(): PresenterFactory<P>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            firstStart = true
            loaderId = BaseActivity.LOADER_ID_VALUE.getAndIncrement()
        } else {
            firstStart = savedInstanceState.getBoolean(BaseActivity.FIRST_START)
            loaderId = savedInstanceState.getInt(BaseActivity.LOADER_ID_KEY)
        }
        injectDependencies()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity.supportLoaderManager.initLoader(loaderId, Bundle.EMPTY, this).startLoading()
    }

    override fun onStart() {
        super.onStart()
        if (presenter == null) {
            needToCallStart.set(true)
        } else {
            doStart()
        }
    }

    @Suppress("UNCHECKED_CAST")
    private fun doStart() {
        presenter?.onViewAttached(this as V)
        presenter?.onStart(firstStart)

        firstStart = false
    }

    override fun onStop() {
        if (presenter != null) {
            presenter?.onStop()
            presenter?.onViewDetached()
        }

        super.onStop()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putBoolean(BaseActivity.FIRST_START, firstStart)
        outState.putInt(BaseActivity.LOADER_ID_KEY, loaderId)
    }

    // LoaderCallbacks

    override fun onCreateLoader(id: Int, args: Bundle): Loader<P> {
        return PresenterLoader(activity, presenterFactory())
    }

    override fun onLoadFinished(loader: Loader<P>, data: P) {
        presenter = data

        if (needToCallStart.compareAndSet(true, false)) {
            doStart()
        }
    }

    override fun onLoaderReset(loader: Loader<P>) {
        presenter = null
    }
}