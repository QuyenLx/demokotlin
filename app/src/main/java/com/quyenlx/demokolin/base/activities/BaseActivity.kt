package com.quyenlx.demokolin.base.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.quyenlx.demokolin.R
import com.quyenlx.demokolin.base.fragment.BaseFragment
import java.util.concurrent.atomic.AtomicInteger

/**
 * Created by QuyenLx on 6/13/2017.
 */
abstract class BaseActivity : AppCompatActivity() {
    companion object {
        val FIRST_START = "first_start"
        val LOADER_ID_KEY = "load_id_key"
        val LOADER_ID_VALUE = AtomicInteger(0)
    }

    private var fragment: BaseFragment? = null


    abstract fun initFragment(): BaseFragment


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.template_base_activity)
        replaceFragment(initFragment())
    }


    private fun replaceFragment(fragment: BaseFragment) {
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, fragment)
                .commit()
    }

    fun addFragment(fragment: BaseFragment) {
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, fragment)
                .addToBackStack(fragment::class.simpleName)
                .commitAllowingStateLoss()
    }

    fun removeFragment() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStackImmediate()
        } else
            finish()
    }

    fun setFragmentCurrent(fragment: BaseFragment) {
        this.fragment = fragment
    }

    override fun onBackPressed() {
        if (fragment?.onBackPressed()!!) {
            return
        }
        super.onBackPressed()
    }

}