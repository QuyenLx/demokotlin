package com.quyenlx.demokolin.base

/**
 * Created by QuyenLx on 6/13/2017.
 */
data class UiInfo(val layoutRes: Int, val titleRes: Int, val menuRes: Int/* = 0 nếu không cần menu*/, val hasBackButton: Boolean)