package com.quyenlx.demokolin.base.loader

import com.quyenlx.demokolin.base.presenter.BasePresenter

/**
 * Created by QuyenLx on 6/13/2017.
 */
interface PresenterFactory<out P : BasePresenter<*>> {
    fun create(): P
}