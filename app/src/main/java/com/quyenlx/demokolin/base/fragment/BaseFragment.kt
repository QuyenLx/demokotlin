package com.quyenlx.demokolin.base.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.quyenlx.demokolin.R
import com.quyenlx.demokolin.base.UiInfo
import com.quyenlx.demokolin.base.activities.BaseActivity
import com.quyenlx.demokolin.helper.showToast

/**
 * Created by QuyenLx on 6/14/2017.
 */
abstract class BaseFragment : Fragment() {
    private val TAG: String = BaseFragment::class.simpleName.toString()
    var mActivity: BaseActivity? = null

    abstract fun getUiInfo(): UiInfo
    abstract fun initView()

    abstract fun onBackPressed(): Boolean
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (activity is BaseActivity) {
            mActivity = activity as BaseActivity
        } else {
            throw ClassCastException("mày cần override BaseActivity -.-")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.template_base_fragment, container, false)
        var ui = getUiInfo()
        val contentView = inflater.inflate(ui.layoutRes, container, false)
        val root: FrameLayout = view.findViewById(R.id.root) as FrameLayout
        val toolbar: Toolbar = view.findViewById(R.id.toolbar) as Toolbar
        root.apply {
            removeAllViews()
            addView(contentView)
        }
        initToolbar(toolbar, ui)
        return view
    }

    fun initToolbar(toolbar: Toolbar, uiInfo: UiInfo) {
        toolbar.setTitle(uiInfo.titleRes)
        if (uiInfo.hasBackButton) {
            toolbar.setNavigationIcon(R.drawable.ic_back_white)
            toolbar.setNavigationOnClickListener { mActivity?.removeFragment() }
        }
        if (uiInfo.menuRes != 0) {
            toolbar.inflateMenu(uiInfo.menuRes)
            toolbar.setOnMenuItemClickListener { menuItem ->
                when (menuItem.itemId) {
                    R.id.action_settings -> {
                        showToast("Clicked!")
                        false
                    }
                    else -> false
                }
            }
        }
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    override fun onStart() {
        super.onStart()
        mActivity?.setFragmentCurrent(this)
    }
}