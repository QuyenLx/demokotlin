package com.quyenlx.demokolin.di.component

import com.quyenlx.demokolin.di.module.*
import dagger.Component
import javax.inject.Singleton

/**
 * Created by QuyenLx on 6/13/2017.
 */
@Singleton
@Component(modules = arrayOf(
        ApplicationModule::class,
        DataModule::class
))
interface ApplicationComponent {
    fun plus(module: BaseModule): BaseComponent
}