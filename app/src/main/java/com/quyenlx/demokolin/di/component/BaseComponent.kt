package com.quyenlx.demokolin.di.component

import com.quyenlx.demokolin.di.module.BaseModule
import com.quyenlx.demokolin.ui.view.fragment.ListDataFragment
import dagger.Subcomponent

/**
 * Created by QuyenLx on 6/14/2017.
 */
@Subcomponent(modules = arrayOf(
        BaseModule::class
)
)
interface BaseComponent {
    fun injectTo(fragment: ListDataFragment)
}