package com.quyenlx.demokolin.di.module

import com.quyenlx.demokolin.App
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by QuyenLx on 6/13/2017.
 */
@Module
class ApplicationModule(private val app: App) {
    @Provides
    @Singleton
    fun provideApplication() = app
}