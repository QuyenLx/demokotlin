package com.quyenlx.demokolin.di.scope

import javax.inject.Scope

/**
 * Created by QuyenLx on 6/13/2017.
 */
@Scope
annotation class ActivityScope