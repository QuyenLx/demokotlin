package com.quyenlx.demokolin.di.module

import com.quyenlx.demokolin.manager.network.api.ApiService
import com.quyenlx.demokolin.ui.interaction.ListInteractor
import com.quyenlx.demokolin.ui.interaction.ListInteractorImpl
import dagger.Module
import dagger.Provides

/**
 * Created by QuyenLx on 6/14/2017.
 */
@Module
class BaseModule {
    @Provides
    fun provideListInteractor(apiService: ApiService): ListInteractor {
        return ListInteractorImpl(apiService)
    }
}