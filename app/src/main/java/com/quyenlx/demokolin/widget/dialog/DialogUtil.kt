package com.quyenlx.demokolin.widget.dialog

import android.app.Dialog
import android.content.Context
import com.quyenlx.demokolin.R
import kotlinx.android.synthetic.main.dialog_confirm.*

/**
 * Created by QuyenLx on 7/3/2017.
 */
class DialogUtil {
    companion object {
        fun showConfirmDialog(context: Context, title: Any, message: Any, negative: () -> Unit, positive: () -> Unit) {
            val dialog = Dialog(context, R.style.MyThemeDialog)
            dialog.setContentView(R.layout.dialog_confirm)
            dialog.setCancelable(false)
            dialog.setCanceledOnTouchOutside(false)
            //Title
            if (title is Int) dialog.titleDialog.setText(title)
            else dialog.titleDialog.text = title as String
            //Message
            if (message is Int) dialog.messageDialog.setText(message)
            else dialog.messageDialog.text = message as String
            //Action Click
            dialog.negativeDialog.setOnClickListener {
                dialog.dismiss()
                negative()
            }
            dialog.positiveDialog.setOnClickListener {
                dialog.dismiss()
                positive()
            }
            dialog.show()
        }

        fun showMessageDialog(context: Context, message: Any, positive: () -> Unit) {
            val dialog = Dialog(context, R.style.MyThemeDialog)
            dialog.setContentView(R.layout.dialog_message)
            dialog.setCancelable(true)
            dialog.setCanceledOnTouchOutside(true)
            //Message
            if (message is Int) dialog.messageDialog.setText(message)
            else dialog.messageDialog.text = message as String
            //Action Click
            dialog.positiveDialog.setOnClickListener {
                dialog.dismiss()
                positive()
            }
            dialog.show()
        }
    }
}