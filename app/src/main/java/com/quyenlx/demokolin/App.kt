package com.quyenlx.demokolin

import android.app.Application
import android.content.Context
import android.support.multidex.MultiDex
import com.quyenlx.demokolin.di.component.ApplicationComponent
import com.quyenlx.demokolin.di.component.DaggerApplicationComponent
import com.quyenlx.demokolin.di.module.ApplicationModule
import com.quyenlx.demokolin.di.module.DataModule

/**
 * Created by QuyenLx on 6/13/2017.
 */
class App : Application() {

    open val applicationComponent: ApplicationComponent by lazy {
        DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this))
                .dataModule(DataModule(BuildConfig.BASE_URL))
                .build()
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}