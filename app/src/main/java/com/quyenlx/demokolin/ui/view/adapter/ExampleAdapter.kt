package com.quyenlx.demokolin.ui.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.quyenlx.demokolin.R
import com.quyenlx.demokolin.model.Item
import kotlinx.android.synthetic.main.item_1.view.*
import kotlinx.android.synthetic.main.item_2.view.*

/**
 * Created by QuyenLx on 6/28/2017.
 */
open class ExampleAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var mItems = mutableListOf<Item>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }


    override
    fun onBindViewHolder(p0: RecyclerView.ViewHolder, p1: Int) {
        if (p0 is ViewBase) {
            p0.bind(mItems[p1])
        }
    }

    override fun getItemCount(): Int {
        return mItems.size
    }

    override fun getItemViewType(position: Int): Int {
        return mItems[position].type
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): RecyclerView.ViewHolder {
        when (p1) {
            1 -> return View1(LayoutInflater.from(parent.context).inflate(R.layout.item_1, parent, false))
            2 -> return View1(LayoutInflater.from(parent.context).inflate(R.layout.item_2, parent, false))
            3 -> return View1(LayoutInflater.from(parent.context).inflate(R.layout.item_3, parent, false))
            4 -> return View1(LayoutInflater.from(parent.context).inflate(R.layout.item_4, parent, false))
            5 -> return View1(LayoutInflater.from(parent.context).inflate(R.layout.item_5, parent, false))
            else -> return View1(LayoutInflater.from(parent.context).inflate(R.layout.item_1, parent, false))
        }
    }

    open class ViewBase(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: Item) {
            itemView.tvItem?.text = item.title
            itemView.tvName?.text = "Title: ${item.title} + Type: ${item.type}"

        }
    }

    class View1(itemView: View) : ViewBase(itemView)

    fun addRow(item: Item) {
        mItems.add(item)
        notifyDataSetChanged()
    }

    fun addRow(item: Item, position: Int) {
        mItems.add(position, item)
        notifyDataSetChanged()
    }

    fun addRow(item: Array<Item>, position: Int) {
        mItems.addAll(position, item.asList())
        notifyDataSetChanged()
    }

    fun removeRow(position: Int) {
        mItems.removeAt(position)
        notifyDataSetChanged()
    }

    fun removeRows(fromIndex: Int, toIndex: Int) {
        mItems.subList(fromIndex, toIndex).clear()
        notifyDataSetChanged()
    }


}

