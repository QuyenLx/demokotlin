package com.quyenlx.demokolin.ui.presenter

import com.quyenlx.demokolin.base.presenter.BasePresenterImp
import com.quyenlx.demokolin.model.Pokemon
import com.quyenlx.demokolin.ui.interaction.ListInteractor
import com.quyenlx.demokolin.ui.view.ListDataView

/**
 * Created by QuyenLx on 6/14/2017.
 */
class ListPresenterImpl(private val interactor: ListInteractor) : BasePresenterImp<ListDataView>(), ListPresenter, ListInteractor.OnGetPokemonListListener {

    private var pokemonList = listOf<Pokemon>()

    override fun onStart(firstStart: Boolean) {
        if (firstStart) {
            getListData()
        } else {
            if (interactor.networkRequestInProgress) {
                view?.showLoadingIndicator()
            }
            view?.showListData(pokemonList)
        }
    }

    override fun onStop() {
        view?.hideLoadingIndicator()
    }

    override fun onPresenterDestroyed() {
        interactor.cancelNetworkRequest()
    }

    // ListPresenter
    override fun getListData() {
        view?.showLoadingIndicator()
        interactor.getPokemonList(this)
    }

    // OnGetPokemonListListener
    override fun onSuccess(pokemonList: List<Pokemon>) {
        this.pokemonList = pokemonList
        view?.showListData(pokemonList)
        view?.hideLoadingIndicator()
        view?.showDoneMessage()
    }

    override fun onFailure() {
        view?.hideLoadingIndicator()
        view?.showErrorMessage()
    }


}