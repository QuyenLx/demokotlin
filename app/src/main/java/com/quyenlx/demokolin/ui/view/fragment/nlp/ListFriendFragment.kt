package com.quyenlx.demokolin.ui.view.fragment.nlp

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.quyenlx.demokolin.R
import com.quyenlx.demokolin.base.UiInfo
import com.quyenlx.demokolin.base.fragment.BaseFragmentNoInject
import com.quyenlx.demokolin.helper.showToast
import com.quyenlx.demokolin.model.Friend
import com.quyenlx.demokolin.ui.view.adapter.ListFriendAdapter
import kotlinx.android.synthetic.main.fragment_list_friend.*
import kotlinx.android.synthetic.main.item_list_friend.view.*

/**
 * Created by QuyenLx on 7/4/2017.
 */
class ListFriendFragment : BaseFragmentNoInject() {
    lateinit var mAdapter: ListFriendAdapter


    override fun getUiInfo(): UiInfo {
        return UiInfo(R.layout.fragment_list_friend, R.string.title_list_friend, 0, true)
    }

    override fun initView() {
        swipe_refresh_layout.apply {
            isRefreshing = false
            setOnRefreshListener {
                showToast("On refresh")
                handler.postDelayed({ isRefreshing = false }, 1000)
            }
        }

        mAdapter = getAdapter()
        recycler_view.apply {
            layoutManager = LinearLayoutManager(activity)
            setHasFixedSize(true)
            adapter = mAdapter
        }
        var listItem = ArrayList<Friend>()
        listItem.add(Friend("Title 1"))
        listItem.add(Friend("Title 2"))
        listItem.add(Friend("Title 3"))
        listItem.add(Friend("Title 4"))
        listItem.add(Friend("Title 5"))
        mAdapter.mItems = listItem

    }

    private fun getAdapter(): ListFriendAdapter {
        mAdapter = createAdapter()
        return mAdapter
    }

    private fun createAdapter(): ListFriendAdapter {
        return object : ListFriendAdapter() {
            override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
                if (holder.itemView.layoutParams == null) {
                    var lp = RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT)
                    holder.itemView.layoutParams = lp
                }
                super.onBindViewHolder(holder, position)
                val item: Friend = mAdapter.mItems[holder.adapterPosition]
                bindItemActionButtonClickEvent(holder.itemView, item, holder.adapterPosition)
            }
        }
    }

    private fun bindItemActionButtonClickEvent(itemView: View, item: Friend, adapterPosition: Int) {
        itemView.setOnClickListener { showToast("itemView is Clicked!") }
        itemView.btnOk.setOnClickListener { showToast("btnOk is Clicked!") }
        itemView.btnCancel.setOnClickListener { showToast("btnCancel is Clicked!") }
    }

    override fun onBackPressed(): Boolean {
        return false
    }
}