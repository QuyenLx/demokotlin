package com.quyenlx.demokolin.ui.interaction

import com.quyenlx.demokolin.base.interaction.BaseInteractor
import com.quyenlx.demokolin.model.Pokemon

/**
 * Created by QuyenLx on 6/14/2017.
 */
interface ListInteractor : BaseInteractor {
    interface OnGetPokemonListListener {
        fun onSuccess(pokemonList: List<Pokemon>)
        fun onFailure()
    }

    var networkRequestInProgress: Boolean

    fun getPokemonList(listener: OnGetPokemonListListener)
    fun cancelNetworkRequest()
}