package com.quyenlx.demokolin.ui.view

import com.quyenlx.demokolin.base.BaseView
import com.quyenlx.demokolin.model.Pokemon

/**
 * Created by QuyenLx on 6/14/2017.
 */
interface ListDataView : BaseView {
    fun showLoadingIndicator()
    fun hideLoadingIndicator()
    fun showListData(pokemonList: List<Pokemon>)
    fun showDoneMessage()
    fun showErrorMessage()
}