package com.quyenlx.demokolin.ui.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.quyenlx.demokolin.R
import com.quyenlx.demokolin.model.Friend
import kotlinx.android.synthetic.main.item_list_friend.view.*

/**
 * Created by QuyenLx on 7/4/2017.
 */
open class ListFriendAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var mItems = mutableListOf<Friend>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getItemCount(): Int {
        return mItems.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is FriendViewHolder)
            holder.bind(mItems[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return FriendViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_list_friend, parent, false))
    }

    class FriendViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: Friend) {
            itemView.tvName?.text = item.title
        }
    }
}