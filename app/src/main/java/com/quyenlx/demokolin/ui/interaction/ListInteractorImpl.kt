package com.quyenlx.demokolin.ui.interaction

import com.quyenlx.demokolin.manager.network.api.ApiService
import com.quyenlx.demokolin.model.Pokemon
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * Created by QuyenLx on 6/14/2017.
 */
class ListInteractorImpl(val apiService: ApiService) : ListInteractor {
    override var networkRequestInProgress = false
    private var subscription: Subscription? = null


    override fun getPokemonList(listener: ListInteractor.OnGetPokemonListListener) {
        networkRequestInProgress = true
        subscription.apply {
            apiService
                    .getPokemonList(150)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .map {
                        it.results.map {
                            val pokemonId = it.url.split("/")[6].toInt()
                            Pokemon(pokemonId, it.name)
                        }
                    }
                    .subscribe({ pokemonList ->
                        networkRequestInProgress = false
                        listener.onSuccess(pokemonList)
                    }, { error ->
                        error.printStackTrace()
                        networkRequestInProgress = false
                        listener.onFailure()
                    })
        }
    }

    override fun cancelNetworkRequest() {
        subscription?.unsubscribe()
    }

}