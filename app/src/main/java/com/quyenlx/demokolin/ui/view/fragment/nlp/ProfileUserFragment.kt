package com.quyenlx.demokolin.ui.view.fragment.nlp

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.quyenlx.demokolin.R
import com.quyenlx.demokolin.base.UiInfo
import com.quyenlx.demokolin.base.fragment.BaseFragmentNoInject
import com.quyenlx.demokolin.helper.showToast
import com.quyenlx.demokolin.model.Item
import com.quyenlx.demokolin.ui.view.adapter.ProfileUserAdapter
import kotlinx.android.synthetic.main.fragment_profile_user.*
import kotlinx.android.synthetic.main.item_profile_user_5.view.*

/**
 * Created by QuyenLx on 7/4/2017.
 */
class ProfileUserFragment : BaseFragmentNoInject() {
    lateinit var mAdapter: ProfileUserAdapter

    override fun getUiInfo(): UiInfo {
        return UiInfo(R.layout.fragment_profile_user, R.string.title_profile_user, 0, true)
    }

    override fun initView() {
        mAdapter = getAdapter()
        recycler_view.apply {
            layoutManager = LinearLayoutManager(activity)
            setHasFixedSize(true)
            adapter = mAdapter
        }
        var listItem = ArrayList<Item>()
        listItem.add(Item("1", 1))
        listItem.add(Item("1", 2))
        listItem.add(Item("1", 3))
        listItem.add(Item("1", 4))
        listItem.add(Item("0", 5))
        listItem.add(Item("0", 5))
        listItem.add(Item("1", 4))
        listItem.add(Item("0", 5))
        listItem.add(Item("0", 5))
        listItem.add(Item("1", 4))
        listItem.add(Item("0", 5))
        listItem.add(Item("0", 5))
        mAdapter.mItems = listItem
    }

    private fun getAdapter(): ProfileUserAdapter {
        mAdapter = createAdapter()
        return mAdapter
    }

    private fun createAdapter(): ProfileUserAdapter {
        return object : ProfileUserAdapter() {
            override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
                if (holder.itemView.layoutParams == null) {
                    var lp = RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT)
                    holder.itemView.layoutParams = lp
                }
                super.onBindViewHolder(holder, position)
                val item: Item = mAdapter.mItems[holder.adapterPosition]
                bindItemActionButtonClickEvent(holder.itemView, item, holder.adapterPosition)
            }
        }
    }

    private fun bindItemActionButtonClickEvent(itemView: View, item: Item, adapterPosition: Int) {
        itemView.imgShowHide?.setOnClickListener {
            showToast("Clicked")
            item.apply {
                if (title.equals("0")) title = "1"
                else title = "0"
            }
            mAdapter.notifyDataSetChanged()
        }

    }

    override fun onBackPressed(): Boolean {
        return false
    }
}