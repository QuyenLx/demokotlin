package com.quyenlx.demokolin.ui.view.fragment

import android.support.v7.widget.LinearLayoutManager
import com.quyenlx.demokolin.R
import com.quyenlx.demokolin.base.UiInfo
import com.quyenlx.demokolin.base.fragment.BaseFragmentInject
import com.quyenlx.demokolin.base.loader.PresenterFactory
import com.quyenlx.demokolin.di.module.BaseModule
import com.quyenlx.demokolin.helper.extension.app
import com.quyenlx.demokolin.helper.showToast
import com.quyenlx.demokolin.model.Pokemon
import com.quyenlx.demokolin.ui.interaction.ListInteractor
import com.quyenlx.demokolin.ui.presenter.ListPresenter
import com.quyenlx.demokolin.ui.presenter.ListPresenterImpl
import com.quyenlx.demokolin.ui.view.ListDataView
import com.quyenlx.demokolin.ui.view.adapter.ListDataAdapter
import kotlinx.android.synthetic.main.fragment_list.*
import javax.inject.Inject

/**
 * Created by QuyenLx on 6/14/2017.
 */
class ListDataFragment : BaseFragmentInject<ListPresenter, ListDataView>(), ListDataView {


    @Inject
    lateinit var interactor: ListInteractor
    lateinit private var mAdapter: ListDataAdapter
    override fun injectDependencies() {
        app.applicationComponent.plus(BaseModule()).injectTo(this)
    }

    override fun presenterFactory(): PresenterFactory<ListPresenter> {
        return object : PresenterFactory<ListPresenter> {
            override fun create(): ListPresenter {
                return ListPresenterImpl(interactor)
            }
        }
    }

    override fun getUiInfo(): UiInfo {
        return UiInfo(R.layout.fragment_list, R.string.title_list, 0, true)
    }

    override fun initView() {
        swipe_refresh_layout.apply {
            setColorSchemeResources(R.color.colorAccent)
            setOnRefreshListener { presenter?.getListData() }
        }
        mAdapter = ListDataAdapter { (id, name) ->
            showToast("Clicked --------------> $id : $name")
        }

        recycler_view.apply {
            adapter = mAdapter
            layoutManager = LinearLayoutManager(activity)
        }
    }

    override fun onBackPressed(): Boolean {
        return false
    }

    //ListDataView
    override fun showLoadingIndicator() {
        swipe_refresh_layout.isRefreshing = true
    }

    override fun hideLoadingIndicator() {
        swipe_refresh_layout.isRefreshing = false
    }

    override fun showListData(pokemonList: List<Pokemon>) {
        mAdapter.pokemonList = pokemonList
    }

    override fun showDoneMessage() {
        showToast(R.string.done)
    }

    override fun showErrorMessage() {
//        alert(R.string.error) {
//            yesButton { toast("Oh…") }
//            noButton {}
//        }.showToast()
    }
}