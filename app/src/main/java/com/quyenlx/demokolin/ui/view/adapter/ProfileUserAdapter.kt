package com.quyenlx.demokolin.ui.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.quyenlx.demokolin.R
import com.quyenlx.demokolin.model.Item
import kotlinx.android.synthetic.main.item_profile_user_5.view.*

/**
 * Created by QuyenLx on 7/5/2017.
 */
open class ProfileUserAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var mItems = mutableListOf<Item>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }


    override fun getItemCount(): Int {
        return mItems.size
    }

    override fun getItemViewType(position: Int): Int {
        return mItems[position].type
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): RecyclerView.ViewHolder {
        when (p1) {
            1 -> return ViewBase(LayoutInflater.from(parent.context).inflate(R.layout.item_profile_user_header, parent, false))
            2 -> return ViewBase(LayoutInflater.from(parent.context).inflate(R.layout.item_profile_user_2, parent, false))
            3 -> return ViewBase(LayoutInflater.from(parent.context).inflate(R.layout.item_profile_user_3, parent, false))
            4 -> return ViewBase(LayoutInflater.from(parent.context).inflate(R.layout.item_profile_user_4, parent, false))
            5 -> return ViewBase(LayoutInflater.from(parent.context).inflate(R.layout.item_profile_user_5, parent, false))
            else -> return ViewBase(LayoutInflater.from(parent.context).inflate(R.layout.item_profile_user_header, parent, false))
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ViewBase) {
            if (getItemViewType(position) == 5) {
                var item = mItems[position]
                if (item.title.equals("0")) {
                    holder.itemView.viewExpand.visibility = View.GONE
                } else {
                    holder.itemView.viewExpand.visibility = View.VISIBLE
                }
            }
        }

    }

    open class ViewBase(itemView: View) : RecyclerView.ViewHolder(itemView)

}