package com.quyenlx.demokolin.ui.view.fragment

import com.quyenlx.demokolin.R
import com.quyenlx.demokolin.base.UiInfo
import com.quyenlx.demokolin.base.fragment.BaseFragment
import com.quyenlx.demokolin.ui.view.fragment.nlp.ContactFragment
import com.quyenlx.demokolin.ui.view.fragment.nlp.ListFriendFragment
import com.quyenlx.demokolin.ui.view.fragment.nlp.ProfileUserFragment
import kotlinx.android.synthetic.main.fragment_main.*

/**
 * Created by QuyenLx on 6/13/2017.
 */
class MainFragment : BaseFragment() {

    override fun onBackPressed(): Boolean {
        return false
    }

    override fun getUiInfo(): UiInfo {
        return UiInfo(R.layout.fragment_main, R.string.title_main, R.menu.menu_main, false)
    }

    override fun initView() {
        btnListFragment?.setOnClickListener { mActivity?.addFragment(ListDataFragment()) }
        btnImagePicker?.setOnClickListener { mActivity?.addFragment(ImagePickerFragment()) }
        btnRecycler?.setOnClickListener { mActivity?.addFragment(RecyclerViewFragment()) }
        btnCustomView?.setOnClickListener { mActivity?.addFragment(CustomViewFragment()) }
        btnListFriend?.setOnClickListener { mActivity?.addFragment(ListFriendFragment()) }
        btnContact?.setOnClickListener { mActivity?.addFragment(ContactFragment()) }
        btnProfileUser?.setOnClickListener { mActivity?.addFragment(ProfileUserFragment()) }
    }

}

