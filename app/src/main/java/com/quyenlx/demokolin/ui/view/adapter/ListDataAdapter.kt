package com.quyenlx.demokolin.ui.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.quyenlx.demokolin.R
import com.quyenlx.demokolin.model.Pokemon
import kotlinx.android.synthetic.main.item_pokemon.view.*

/**
 * Created by QuyenLx on 6/14/2017.
 */
class ListDataAdapter(val listener: (Pokemon) -> Unit) : RecyclerView.Adapter<ListDataAdapter.ViewHolder>() {
    var pokemonList = listOf<Pokemon>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }


    override
    fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_pokemon, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = pokemonList[position]
        holder.bind(item, listener)
    }

    override fun getItemCount() = pokemonList.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(pokemon: Pokemon, listener: (Pokemon) -> Unit) {
            itemView.pokemon_name_text_view.text = pokemon.name
            itemView.setOnClickListener { listener(pokemon) }
        }
    }

}