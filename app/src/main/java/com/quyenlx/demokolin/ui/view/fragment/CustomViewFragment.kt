package com.quyenlx.demokolin.ui.view.fragment

import com.quyenlx.demokolin.R
import com.quyenlx.demokolin.base.UiInfo
import com.quyenlx.demokolin.base.fragment.BaseFragmentNoInject
import com.quyenlx.demokolin.helper.showToast
import com.quyenlx.demokolin.widget.dialog.DialogUtil
import kotlinx.android.synthetic.main.fragment_custom_view.*

/**
 * Created by QuyenLx on 7/3/2017.
 */
class CustomViewFragment : BaseFragmentNoInject() {
    override fun getUiInfo(): UiInfo {
        return UiInfo(R.layout.fragment_custom_view, R.string.title_custom_view, 0, true);
    }

    override fun initView() {
        btnShowConfirmDialog.setOnClickListener {
            DialogUtil.showConfirmDialog(
                    context,
                    "Title", "Message nài",
                    { showToast("Negative") },
                    { showToast("Positive") }
            )
        }

        btnShowMessageDialog.setOnClickListener {
            DialogUtil.showMessageDialog(
                    context,
                    "Message nài",
                    { showToast("Negative") }
            )
        }
    }

    override fun onBackPressed(): Boolean {
        return false
    }
}