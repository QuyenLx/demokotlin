package com.quyenlx.demokolin.ui.presenter

import com.quyenlx.demokolin.base.presenter.BasePresenter
import com.quyenlx.demokolin.ui.view.ListDataView

/**
 * Created by QuyenLx on 6/14/2017.
 */
interface ListPresenter : BasePresenter<ListDataView> {
    fun getListData()
}