package com.quyenlx.demokolin.ui.view.fragment

import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.view.View
import com.quyenlx.demokolin.R
import com.quyenlx.demokolin.base.UiInfo
import com.quyenlx.demokolin.base.fragment.BaseFragment
import com.quyenlx.demokolin.helper.*
import com.quyenlx.demokolin.helper.showToast
import com.quyenlx.demokolin.model.Item
import com.quyenlx.demokolin.ui.view.adapter.ExampleAdapter
import kotlinx.android.synthetic.main.fragment_recycler.*
import kotlinx.android.synthetic.main.item_2.view.*
import kotlinx.android.synthetic.main.item_4.view.*


/**
 * Created by QuyenLx on 6/28/2017.
 */
class RecyclerViewFragment : BaseFragment() {
    private val TAG: String = RecyclerViewFragment::class.simpleName.toString()
    lateinit var mAdapter: ExampleAdapter
    private var countRow: Int = 1


    override fun getUiInfo(): UiInfo {
        return UiInfo(R.layout.fragment_recycler, R.string.title_recycler, 0, true)
    }

    override fun initView() {
        mAdapter = getExampleAdapter()
        recycler_view.apply {
            layoutManager = LinearLayoutManager(activity)
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
            setHasFixedSize(true)
            adapter = mAdapter
        }
        var listItem = ArrayList<Item>()
//        listItem.add(Item("1", 1))
//        listItem.add(Item("2", 4))
        listItem.add(Item("${countRow++}", 2))

        mAdapter.mItems = listItem
    }

    override fun onBackPressed(): Boolean {
        return false
    }

    fun getExampleAdapter(): ExampleAdapter {
        mAdapter = createAdapter()
        return mAdapter
    }

    private fun createAdapter(): ExampleAdapter {
        return object : ExampleAdapter() {
            override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
                if (holder.itemView.layoutParams == null) {
                    var lp = RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT)
                    holder.itemView.layoutParams = lp
                }
                super.onBindViewHolder(holder, position)
                val item: Item = mAdapter.mItems[holder.adapterPosition]
                bindItemActionButtonClickEvent(holder.itemView, item, holder.adapterPosition)
            }
        }
    }

    private fun bindItemActionButtonClickEvent(itemView: View, item: Item, position: Int) {
        itemView.setOnClickListener { showToast("Clicked!") }
        //item_4
        /* itemView.btnAddRow?.setOnClickListener {
             d(TAG, "btn btnAddRow clicked")
             mAdapter.addRow(Item("${count++}", 2))
             recycler_view.scrollToPosition(mAdapter.mItems.size - 1)
         }
         itemView.btnAddSubRow?.setOnClickListener { i(TAG, "btn btnAddSubRow clicked") }*/
        //item_2
        itemView.btnMore?.setOnClickListener {
            showPopupMenu(itemView.btnMore, item, position)
        }

    }

    private fun showPopupMenu(view: View, item: Item, position: Int) {
        var index = position
        var count = 1
        var popupMenu = PopupMenu(context, view)
        popupMenu.menuInflater.inflate(R.menu.menu_item, popupMenu.menu)
        try {
            val fields = popupMenu::class.java.declaredFields
            for (field in fields) {
                if ("mPopup" == field.name) {
                    field.isAccessible = true
                    val menuPopupHelper = field.get(popupMenu)
                    val classPopupHelper = Class.forName(menuPopupHelper.javaClass.name)
                    val setForceIcons = classPopupHelper.getMethod(
                            "setForceShowIcon", Boolean::class.javaPrimitiveType)
                    setForceIcons.invoke(menuPopupHelper, true)
                    break
                }
            }
        } catch (ex: Exception) {
            e(TAG, "error", ex)
        }
        popupMenu.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.menu_delete -> {
                    if (item.type == 2) {
                        if (position == mAdapter.mItems.size - 1)
                            count = 1
                        else {
                            while (++index < mAdapter.mItems.size && mAdapter.mItems[index].type == 5) {
                                count++
                            }
                        }
                        mAdapter.removeRows(position, position + count)
                    } else
                        mAdapter.removeRow(position)
                }
                R.id.menu_add_sub -> {
                    while (++index < mAdapter.mItems.size && mAdapter.mItems[index].type == 5) {
                        count++
                    }
                    mAdapter.addRow(Item("Sub row", 5), position + count)
                    recycler_view.scrollToPosition(position + count)
                }
                R.id.menu_add_mul -> {
                    while (++index < mAdapter.mItems.size && mAdapter.mItems[index].type == 5) {
                        count++
                    }
                    mAdapter.addRow(arrayOf(Item("1", 5), Item("2", 5), Item("3", 5)), position + count)
                    recycler_view.scrollToPosition(position + count + 2)
                }
                R.id.menu_add -> {
                    mAdapter.addRow(Item("${countRow++}", 2))
                    recycler_view.scrollToPosition(mAdapter.mItems.size - 1)
                }
            }
            true
        }

        popupMenu.show()
    }
}