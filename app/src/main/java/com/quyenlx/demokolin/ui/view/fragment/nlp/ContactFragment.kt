package com.quyenlx.demokolin.ui.view.fragment.nlp

import android.Manifest
import android.content.ContentResolver
import android.provider.ContactsContract
import com.quyenlx.demokolin.R
import com.quyenlx.demokolin.base.UiInfo
import com.quyenlx.demokolin.base.fragment.BaseFragmentNoInject
import com.quyenlx.imagepicker.RxPermissionCompat
import kotlinx.android.synthetic.main.fragment_contact.*

/**
 * Created by QuyenLx on 7/5/2017.
 */
class ContactFragment : BaseFragmentNoInject() {
    override fun getUiInfo(): UiInfo {
        return UiInfo(R.layout.fragment_contact, R.string.title_contact, 0, true)
    }

    override fun initView() {
        RxPermissionCompat.Builder(context)
                .addPermissions(arrayOf(Manifest.permission.READ_CONTACTS))
                .addRequestPermissionCallback { fetchContacts() }
                .build()
    }

    override fun onBackPressed(): Boolean {
        return false
    }

    fun fetchContacts() {
        var phoneNumber: String? = null
        var email: String? = null

        val CONTENT_UIR = ContactsContract.Contacts.CONTENT_URI
        var _ID = ContactsContract.Contacts._ID
        var DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME
        var HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER

        var PhoneContent_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI
        var Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID
        var NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER

        var EmailCONTENT_URI = ContactsContract.CommonDataKinds.Email.CONTENT_URI
        var Email_CONTACT_ID = ContactsContract.CommonDataKinds.Email.CONTACT_ID
        var DATA = ContactsContract.CommonDataKinds.Email.DATA

        var output = StringBuffer()

        var contentResolver: ContentResolver = context.contentResolver
        var cursor = contentResolver.query(CONTENT_UIR, null, null, null, null)
        if (cursor.count > 0) {
            while (cursor.moveToNext()) {
                var contact_id = cursor.getString(cursor.getColumnIndex(_ID))
                var name = cursor.getString(cursor.getColumnIndex(DISPLAY_NAME))
                var hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(HAS_PHONE_NUMBER)))
                if (hasPhoneNumber > 0) {
                    output.append("\nFirst Name: $name")

                    // Query and loop for every phone number of the contact
                    var phoneCursor = contentResolver.query(PhoneContent_URI, null, Phone_CONTACT_ID + " = ?", arrayOf(contact_id), null)
                    while (phoneCursor.moveToNext()) {
                        phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER))
                        output.append("\nPhone number: $phoneNumber")
                    }
                    phoneCursor.close()
                    // Query and loop for every email of the contact
                    var mailCursor = contentResolver.query(EmailCONTENT_URI, null, Email_CONTACT_ID + " = ?", arrayOf(contact_id), null)
                    while (mailCursor.moveToNext()) {
                        email = mailCursor.getString(mailCursor.getColumnIndex(DATA))
                        output.append("\nEmail : $email")
                    }
                    mailCursor.close()
                }
                output.append("\n")
            }
            tvContact.text = output
        }
    }
}