package com.quyenlx.demokolin.ui.view.fragment

import android.Manifest
import android.net.Uri
import android.widget.Toast
import com.quyenlx.demokolin.R
import com.quyenlx.demokolin.base.UiInfo
import com.quyenlx.demokolin.base.fragment.BaseFragmentNoInject
import com.quyenlx.demokolin.helper.extension.setImageWithPicasso
import com.quyenlx.imagepicker.RxImagePicker
import com.quyenlx.imagepicker.RxPermissionCompat
import com.quyenlx.imagepicker.Sources
import kotlinx.android.synthetic.main.fragment_image_picker.*

/**
 * Created by QuyenLx on 6/15/2017.
 */
class ImagePickerFragment : BaseFragmentNoInject() {
    override fun getUiInfo(): UiInfo {
        return UiInfo(R.layout.fragment_image_picker, R.string.title_image_picker, 0, true)
    }

    override fun initView() {
        btnGallery.setOnClickListener {
            //            RxImagePicker.with(context)
//                    .requestImage(Sources.GALLERY)
//                    ?.subscribe({ uri -> onImagePicked(uri) }, { error -> Throwable(error) })
            RxPermissionCompat.Builder(context)
                    .addPermissions(arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE))
                    .addRequestPermissionCallback({
                        Toast.makeText(context, "Enable", Toast.LENGTH_SHORT).show()
                    })
                    .build()
        }
        btnCamera.setOnClickListener {
            RxImagePicker.with(context)
                    .requestImage(Sources.CAMERA)
                    ?.subscribe({ uri -> onImagePicked(uri) }, { error -> Throwable(error) })
        }
    }

    override fun onBackPressed(): Boolean {
        return false
    }

    private fun onImagePicked(uri: Uri?) {
        img.setImageWithPicasso(uri, context)
    }

}