package com.quyenlx.demokolin.ui.view.activities

import com.quyenlx.demokolin.base.activities.BaseActivity
import com.quyenlx.demokolin.base.fragment.BaseFragment
import com.quyenlx.demokolin.ui.view.fragment.MainFragment

class MainActivity : BaseActivity() {
    override fun initFragment(): BaseFragment {
        return MainFragment()
    }

}
