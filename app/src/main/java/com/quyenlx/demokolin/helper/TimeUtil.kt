package com.ominext.basekotlin.library.helper

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.Locale


/**
 * Created by QuyenLx on 6/29/2017.
 */
object TimeUtil {
    val DEFAULT_SDF = "yyyy-MM-dd HH:mm:ss"

    //region: Time Constant
    val MSEC = 1
    val SEC = 1000
    val MIN = 60000
    val HOUR = 3600000
    val DAY = 86400000

    enum class TimeUnit {
        MSEC,
        SEC,
        MIN,
        HOUR,
        DAY
    }
    //endregion

    /**
     * @param template
     * @return SimpleDateFormat
     */
    fun string2SDF(template: String): SimpleDateFormat {
        return SimpleDateFormat(template, Locale.getDefault())
    }

    /**
     * @param milliseconds
     * @return String : yyyy-MM-dd HH:mm:ss (default)
     */
    fun milliseconds2String(milliseconds: Long): String {
        return milliseconds2String(milliseconds, DEFAULT_SDF)
    }

    /**
     * @param milliseconds
     * @param format
     * @return String
     */
    fun milliseconds2String(milliseconds: Long, format: String): String {
        return string2SDF(format).format(Date(milliseconds))
    }

    /**
     * @param time
     * @return Long
     */
    fun string2Milliseconds(time: String): Long {
        return string2Milliseconds(time, DEFAULT_SDF)
    }

    /**
     * @param time
     * @param format
     * @return Long
     */
    fun string2Milliseconds(time: String, format: String): Long {
        try {
            return string2SDF(format).parse(time).time
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return -1
    }

    /**
     * @param time
     * @return Date
     */
    fun string2Date(time: String): Date {
        return string2Date(time, DEFAULT_SDF)
    }

    /**
     * @param time
     * @param format
     * @return Date
     */
    fun string2Date(time: String, format: String): Date {
        return Date(string2Milliseconds(time, format))
    }

    /**
     * @param time
     * @return String : yyyy-MM-dd HH:mm:ss (default)
     */
    fun date2String(time: Date): String {
        return date2String(time, DEFAULT_SDF)
    }

    /**
     * @param time
     * @param format
     * @return String
     */
    fun date2String(time: Date, format: String): String {
        return string2SDF(format).format(time)
    }

    /**
     * @param time
     * @return Long
     */
    fun date2Milliseconds(time: Date): Long {
        return time.time
    }

    /**
     * @param milliseconds
     * @return Date
     */
    fun milliseconds2Date(milliseconds: Long): Date {
        return Date(milliseconds)
    }

    private fun milliseconds2Unit(milliseconds: Long, unit: TimeUnit): Long {
        when (unit) {
            TimeUnit.MSEC -> return milliseconds / MSEC
            TimeUnit.SEC -> return milliseconds / SEC
            TimeUnit.MIN -> return milliseconds / MIN
            TimeUnit.HOUR -> return milliseconds / HOUR
            TimeUnit.DAY -> return milliseconds / DAY
        }
        return -1
    }

    /*
     * ---------------------------------------------------------------------
     *                         Tính khoảng thời gian
     * ---------------------------------------------------------------------
     */
    fun getIntervalTime(time0: String, time1: String, unit: TimeUnit): Long {
        return getIntervalTime(time0, time1, unit, DEFAULT_SDF)
    }


    fun getIntervalTime(time0: String, time1: String, unit: TimeUnit, format: String): Long {
        return milliseconds2Unit(Math.abs(string2Milliseconds(time0, format) - string2Milliseconds(time1, format)), unit)
    }

    fun getIntervalTime(time0: Date, time1: Date, unit: TimeUnit): Long {
        return milliseconds2Unit(Math.abs(date2Milliseconds(time1) - date2Milliseconds(time0)), unit)
    }

    fun getIntervalByNow(time: String, unit: TimeUnit, format: String): Long {
        return getIntervalTime(getCurTimeString(), time, unit, format)
    }

    fun getIntervalByNow(time: Date, unit: TimeUnit): Long {
        return getIntervalTime(getCurTimeDate(), time, unit)
    }
    /*
     * ---------------------------------------------------------------------
     *                         lấy thời gian hiện tại
     * ---------------------------------------------------------------------
     */

    fun getCurTimeMills(): Long {
        return System.currentTimeMillis()
    }

    fun getCurTimeString(): String {
        return date2String(Date())
    }

    fun getCurTimeString(format: String): String {

        return date2String(Date(), format)
    }

    fun getCurTimeDate(): Date {
        return Date()
    }

    fun getIntervalByNow(time: String, unit: TimeUnit): Long {
        return getIntervalByNow(time, unit, DEFAULT_SDF)
    }

    /*
    * ---------------------------------------------------------------------
    *                         Kiểm tra năm nhuận
    * ---------------------------------------------------------------------
    */
    fun isLeapYear(year: Int): Boolean {
        return year % 4 == 0 && year % 100 != 0 || year % 400 == 0
    }

    /*
    * ---------------------------------------------------------------------
    *                         ngày xxx là thứ yyy
    * ---------------------------------------------------------------------
    */
    fun getWeek(time: String): String {
        return SimpleDateFormat("EEEE", Locale.getDefault()).format(string2Date(time))
    }

    fun getWeek(time: String, format: String): String {
        return SimpleDateFormat("EEEE", Locale.getDefault()).format(string2Date(time, format))
    }

    fun getWeek(time: Date): String {
        return SimpleDateFormat("EEEE", Locale.getDefault()).format(time)
    }
    /*
    * ---------------------------------------------------------------------
    *                         ngày xxx ở vị trí yyy (1..7)
    * ---------------------------------------------------------------------
    */

    fun getWeekIndex(time: String): Int {
        val date = string2Date(time)
        return getWeekIndex(date)
    }

    fun getWeekIndex(time: String, format: String): Int {
        val date = string2Date(time, format)
        return getWeekIndex(date)
    }

    fun getWeekIndex(time: Date): Int {
        val cal = Calendar.getInstance()
        cal.time = time
        return cal.get(Calendar.DAY_OF_WEEK)
    }

    /*
    * ---------------------------------------------------------------------
    *                         ngày xxx là tuần thứ  yyy (1..5) của tháng
    * ---------------------------------------------------------------------
    */

    fun getWeekOfMonth(time: String): Int {
        val date = string2Date(time)
        return getWeekOfMonth(date)
    }

    fun getWeekOfMonth(time: String, format: String): Int {
        val date = string2Date(time, format)
        return getWeekOfMonth(date)
    }

    fun getWeekOfMonth(time: Date): Int {
        val cal = Calendar.getInstance()
        cal.time = time
        return cal.get(Calendar.WEEK_OF_MONTH)
    }

    /*
    * ---------------------------------------------------------------------
    *                         ngày xxx là tuần thứ  yyy (1..5) của năm
    * ---------------------------------------------------------------------
    */
    fun getWeekOfYear(time: String): Int {
        val date = string2Date(time)
        return getWeekOfYear(date)
    }

    fun getWeekOfYear(time: String, format: String): Int {
        val date = string2Date(time, format)
        return getWeekOfYear(date)
    }

    fun getWeekOfYear(time: Date): Int {
        val cal = Calendar.getInstance()
        cal.time = time
        return cal.get(Calendar.WEEK_OF_YEAR)
    }
}