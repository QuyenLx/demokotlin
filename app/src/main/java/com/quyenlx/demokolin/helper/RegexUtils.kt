package com.ominext.basekotlin.library.helper

import android.text.TextUtils
import java.util.regex.Pattern

/**
 * Created by QuyenLx on 6/29/2017.
 */
object RegexUtils {
    val REGEX_MOBILE_SIMPLE = "^[1]\\d{10}$"
    val REGEX_MOBILE_EXACT = "^((13[0-9])|(14[5,7])|(15[0-3,5-9])|(17[0,3,5-8])|(18[0-9])|(147))\\d{8}$"
    val REGEX_TEL = "^0\\d{2,3}[- ]?\\d{7,8}"
    val REGEX_EMAIL = "^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$"
    val REGEX_URL = "http(s)?://([\\w-]+\\.)+[\\w-]+(/[\\w-./?%&=]*)?"
    val REGEX_USERNAME = "^[\\w\\u4e00-\\u9fa5]{6,20}(?<!_)$"
    val REGEX_DATE = "^(?:(?!0000)[0-9]{4}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-8])|(?:0[13-9]|1[0-2])-(?:29|30)|(?:0[13578]|1[02])-31)|(?:[0-9]{2}(?:0[48]|[2468][048]|[13579][26])|(?:0[48]|[2468][048]|[13579][26])00)-02-29)$"

    fun isMobileSimple(string: String): Boolean {
        return isMatch(REGEX_MOBILE_SIMPLE, string)
    }

    fun isMobileExact(string: String): Boolean {
        return isMatch(REGEX_MOBILE_EXACT, string)
    }

    fun isTel(string: String): Boolean {
        return isMatch(REGEX_TEL, string)
    }

    fun isEmail(string: String): Boolean {
        return isMatch(REGEX_EMAIL, string)
    }

    fun isURL(string: String): Boolean {
        return isMatch(REGEX_URL, string)
    }

    fun isUsername(string: String): Boolean {
        return isMatch(REGEX_USERNAME, string)
    }

    fun isDate(string: String): Boolean {
        return isMatch(REGEX_DATE, string)
    }

    fun isMatch(regex: String, string: String): Boolean {
        return !TextUtils.isEmpty(string) && Pattern.matches(regex, string)
    }
}