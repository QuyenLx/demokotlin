package com.quyenlx.demokolin.helper

import android.util.Log
import com.quyenlx.demokolin.BuildConfig

/**
 * Created by QuyenLx on 6/27/2017.
 */

/*
 *----------------------------------------------------------------------------------------
 *  Message Type  : Info
 *  Display color : Green
 *----------------------------------------------------------------------------------------
 */


fun i(tag: String, message: String) {
    if (BuildConfig.DEBUG) {
        Log.i(tag, message)
    }
}

fun i(tag: String, message: String, throwable: Throwable) {
    if (BuildConfig.DEBUG) {
        Log.i(tag, message, throwable)
    }
}

/*
 *----------------------------------------------------------------------------------------
 *  Message Type  : Error
 *  Display color : Red
 *----------------------------------------------------------------------------------------
 */

fun e(tag: String, message: String) {
    if (BuildConfig.DEBUG) {
        Log.e(tag, message)
    }
}

fun e(tag: String, message: String, throwable: Throwable) {
    if (BuildConfig.DEBUG) {
        Log.e(tag, message, throwable)
    }
}

/*
 *----------------------------------------------------------------------------------------
 *  Message Type  : Warm
 *  Display color : Orange
 *----------------------------------------------------------------------------------------
 */

fun w(tag: String, message: String) {
    if (BuildConfig.DEBUG) {
        Log.w(tag, message)
    }
}

fun w(tag: String, message: String, throwable: Throwable) {
    if (BuildConfig.DEBUG) {
        Log.w(tag, message, throwable)
    }
}

/*
 *----------------------------------------------------------------------------------------
 *  Message Type  : Debug
 *  Display color : Blue
 *----------------------------------------------------------------------------------------
 */

fun d(tag: String, message: String) {
    if (BuildConfig.DEBUG) {
        Log.d(tag, message)
    }
}

fun d(tag: String, message: String, throwable: Throwable) {
    if (BuildConfig.DEBUG) {
        Log.d(tag, message, throwable)
    }
}