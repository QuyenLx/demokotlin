package com.quyenlx.demokolin.helper

import android.app.Activity
import android.content.Context
import android.support.v4.app.Fragment
import android.widget.Toast

/**
 * Created by QuyenLx on 6/27/2017.
 */


fun Activity.showToast(message: String) {
    show(this, message)
}

fun Activity.showToast(message: Int) {
    show(this, message)
}

fun Fragment.showToast(message: String) {
    show(this.context, message)
}

fun Fragment.showToast(message: Int) {
    show(this.context, message)
}

fun showToast(context: Context, message: String) {
    show(context, message)
}

fun showToast(context: Context, message: Int) {
    show(context, message)
}


/*
 * -----------------------------------------------------------------------------
 *  Private methods
 * -----------------------------------------------------------------------------
 */
private fun show(context: Context, message: String) {
    Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
}

private fun show(context: Context, message: Int) {
    Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
}
