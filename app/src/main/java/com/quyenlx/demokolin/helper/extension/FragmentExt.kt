package com.quyenlx.demokolin.helper.extension

import android.support.v4.app.Fragment
import com.quyenlx.demokolin.App

/**
 * Created by QuyenLx on 6/13/2017.
 */
val Fragment.app: App
    get() = activity.application as App
