package com.quyenlx.demokolin.helper.extension

import android.content.Context
import android.net.Uri
import android.widget.ImageView
import com.squareup.picasso.Picasso

/**
 * Created by QuyenLx on 6/15/2017.
 */
fun ImageView.setImageWithPicasso(uri: Uri?, context: Context) {
    Picasso.with(context).load(uri).into(this)
}