package com.quyenlx.demokolin.helper.extension

import android.support.v7.app.AppCompatActivity
import com.quyenlx.demokolin.App

/**
 * Created by QuyenLx on 6/13/2017.
 */

val AppCompatActivity.app: App
    get() = application as App

