package com.quyenlx.demokolin.model

/**
 * Created by QuyenLx on 6/14/2017.
 */
data class ResponsePokemonList(val count: Int,
                               val next: String,
                               val previous: Boolean,
                               val results: List<ResponsePokemonListName>)