package com.quyenlx.demokolin.model

/**
 * Created by QuyenLx on 7/4/2017.
 */
data class Friend(val title: String)