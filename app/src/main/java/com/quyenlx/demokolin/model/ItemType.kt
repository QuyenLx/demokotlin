package com.quyenlx.demokolin.model

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter
import java.io.IOException

/**
 * Created by QuyenLx on 6/28/2017.
 */
enum class ItemType(type: Int) {
    TOP(1),
    CENTER(2),
    CENTER_EDIT_TEXT(3),
    BOTTOM(4);

    var apiValue: Int = -1

    init {
        this.apiValue = type
    }

    companion object {
        fun fromApiValue(value: Int): ItemType? {
            return ItemType.values().firstOrNull { it.apiValue == value }
        }
    }

    class Adapter : TypeAdapter<ItemType>() {
        @Throws(IOException::class)
        override fun write(out: JsonWriter, value: ItemType?) {
            if (value == null) {
                out.nullValue()
                return
            }
            out.value(value.apiValue.toString())
        }

        @Throws(IOException::class)
        override fun read(reader: JsonReader): ItemType? {
            val token = reader.peek()
            if (token == JsonToken.NULL) {
                reader.nextNull()
                return null
            }
            return ItemType.fromApiValue(reader.nextInt())
        }
    }
}