package com.quyenlx.demokolin.model

/**
 * Created by QuyenLx on 6/14/2017.
 */
data class PokemonSprites(
        val frontDefault: String = "",
        val frontShiny: String = "",
        val frontFemale: String = "",
        val frontShinyFemale: String = "",
        val backDefault: String = "",
        val backShiny: String = "",
        val backFemale: String = "",
        val backShinyFemale: String = ""
)