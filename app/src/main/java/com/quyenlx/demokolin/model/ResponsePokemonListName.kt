package com.quyenlx.demokolin.model

/**
 * Created by QuyenLx on 6/14/2017.
 */
class ResponsePokemonListName(
        val name: String,
        val url: String
)