package com.quyenlx.demokolin.model

/**
 * Created by QuyenLx on 6/28/2017.
 */
data class Item(var title: String, val type: Int)

