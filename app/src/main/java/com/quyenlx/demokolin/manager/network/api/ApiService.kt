package com.quyenlx.demokolin.manager.network.api

import com.quyenlx.demokolin.model.ResponsePokemonList
import retrofit2.http.GET
import retrofit2.http.Query
import rx.Observable

/**
 * Created by QuyenLx on 6/13/2017.
 */
interface ApiService {
    @GET("pokemon")
    fun getPokemonList(@Query("limit") limit: Int): Observable<ResponsePokemonList>
}